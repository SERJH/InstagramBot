from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from datetime import date
from time import sleep
from Scripts.testLogs import TestLogs

class InstaBot(object):

	def __init__(self, username, password, wait_time = 25):
		
		self.username = username
		self.password = password
		self.wait_time = wait_time
		self.browser = None
		self.body_elem = None

	def loadUsers(self, amount, multiplier, user, action, maximum):

		self.amount = amount
		self.multiplier = multiplier
		self.user = user
		self.action = action
		self.maximum = maximum
		self.following_element = None
		self.followers_element = None

		self.body_elem = self.browser.find_element_by_tag_name("body")

		print("Loading users...")
		ulElement = self.browser.find_element_by_xpath("//div/div/div/div/div/ul/div")
		sleep(2)
		liElements = ulElement.find_elements_by_tag_name("li")
		liCount = len(liElements)

		errorCount = 0
		lastCount = liCount

		ulElement.click()

		# Start the load loop

		for i in range(100):
			
			# Print current stats

			print("Amount of elements:", liCount)
			print("Desired amount:", self.amount * self.multiplier)
			print()

			# If liCount is staying the same

			if (liCount == lastCount):

				errorCount += 1

				# If it occured three times in a row

				if (errorCount >= 3):

					errorCount = 0

					# Reload the page

					print("Error. Restarting process...")
					self.browser.get('https://www.instagram.com/' + self.user + '/')

					if action == "follow":
						
						self.followers_element = self.browser.find_element_by_xpath("//article/header/section/ul/li[2]")
						self.followers_element.click()

						sleep(2)

						ulElement = self.browser.find_element_by_xpath("//div/div/div/div/div/ul/div")
						sleep(2)
						liElements = ulElement.find_elements_by_tag_name("li")
						liCount = len(liElements)

						ulElement.click()

						self.body_elem = self.browser.find_element_by_tag_name("body")

					elif action == "unfollow":

						self.following_element = self.browser.find_element_by_xpath("//article/header/section/ul/li[3]")
						self.following_element.click()

						sleep(2)

						ulElement = self.browser.find_element_by_xpath("//div/div/div/div/div/ul/div")
						sleep(2)
						liElements = ulElement.find_elements_by_tag_name("li")
						liCount = len(liElements)

						ulElement.click()

						self.body_elem = self.browser.find_element_by_tag_name("body")

			# Set the lastCount variable equal to the latest value of liCount before its re-assignment

			lastCount = liCount

			# If user amount isn't enough yet

			if liCount < (self.amount * self.multiplier) and liCount < maximum:

				for j in range(5):
					
					self.body_elem.send_keys(Keys.END)
					sleep(2)

				ulElement = self.browser.find_element_by_xpath("//div/div/div/div/div/ul/div")
				sleep(2)
				liElements = ulElement.find_elements_by_tag_name("li")
				liCount = len(liElements)

			else:

				print("Desired amount of users complete")
				self.body_elem.send_keys(Keys.HOME)
				break

	def logIn(self):


		# Starting a new browser session

		print("███████████████████████████████████████████████████████████████████████████████")
		print("█                                 STARTING UP!                                █")
		print("███████████████████████████████████████████████████████████████████████████████")

		options = Options()
		options.add_argument("--headless") # Runs Chrome in headless mode.
		options.add_argument('--no-sandbox') # Bypass OS security model
		options.add_argument('--disable-gpu')  # applicable to windows os only
		options.add_argument('start-maximized') # 
		options.add_argument('disable-infobars')
		options.add_argument("--disable-extensions")
		self.browser = webdriver.Chrome(chrome_options=options, executable_path=r'C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/ChromeDriver/chromedriver.exe')
		#self.browser = webdriver.Chrome('C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/ChromeDriver/chromedriver.exe')
		#self.browser.maximize_window()

		# Navigating to a webpage


		self.browser.get('https://www.instagram.com')


		# Find log in

		sleep(5)

		login_element = self.browser.find_element_by_xpath('//article/div/div/p/a[text()="Log in"]')


		# Click it


		login_element.click()


		# Find forms and enter data


		input_username = self.browser.find_element_by_name('username')
		input_password = self.browser.find_element_by_name('password')

		ActionChains(self.browser)\
			.move_to_element(input_username).click()\
			.send_keys(self.username)\
			.move_to_element(input_password).click()\
			.send_keys(self.password)\
			.perform()


		# Find log in button and click it


		login_button = self.browser.find_element_by_xpath('//form/span/button[text()="Log in"]')

		ActionChains(self.browser)\
			.move_to_element(login_button)\
			.click().perform()

		sleep(5)

		self.browser.implicitly_wait(self.wait_time)

	def unfollow_users(self, amount, sleepTime, sleepInterval):

		self.amount = amount
		self.sleepTime = sleepTime
		self.sleepInterval = sleepInterval
		self.following_element = None

		amountCheck = self.amount;

		t = TestLogs()

		# Go to user's profile


		self.browser.get("https://www.instagram.com/" + self.username + "/")


		# Search for 'following' button and click it

		
		following_element = self.browser.find_element_by_xpath("//article/header/section/ul/li[3]")
		following_element.click()

		followingAmount = following_element.find_element_by_xpath(".//a/span").text	
		followingAmount = int(t.removeCharFromString(followingAmount, ','))
		print(followingAmount)


		# Load as much users as the amount variable says


		self.loadUsers(self.amount, 3, self.username, "unfollow", followingAmount)

		ulElement = self.browser.find_element_by_xpath("//div/div/div/div/div/ul/div")
		sleep(2)
		liElements = ulElement.find_elements_by_tag_name("li")
		liCount = len(liElements)


		# Iterate through the list of users and open the log file to check and update it after each loop


		log = open("C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/Logs/"+ self.username + ".txt", "r")
		today = date.today()
		unfollow_count = 0
		element = 0
		sleepy = 0

		print("\n --- Starting unfollow session --- \n")

		while unfollow_count < self.amount:

			# If there is no more elements and the loop hasn't concluded yet, load new ones and keep going

			if element == (len(liElements) - 1):

				self.loadUsers(len(liElements) + 100, 1, self.user, "unfollow", followingAmount)

				ulElement = self.browser.find_element_by_xpath("//div/div/div/div/div/ul/div")
				sleep(2)
				liElements = ulElement.find_elements_by_tag_name("li")
				liCount = len(liElements)

			# Find the current element username and check the log to see when was he followed

			elementUsername = liElements[element].find_element_by_xpath(".//div/div/div/div/a")
			sleep(2)

			followedDate = t.getDateByUsername(elementUsername.text, self.username)
			
			# If we have data about that user (has been followed using this script)

			if followedDate != '':

				print(followedDate)
				daysDifference = t.compareDateWithToday(followedDate)

				# Compare the date he was followed with todays date, if the difference is bigger than two, the person will be unfollowed

				if daysDifference < 2:

					print("Aún no se puede dejar de seguir al usuario " + elementUsername.text + "\n")
					element += 1
					continue

				else:

					# More than two days, user will be unfollowed and erased from the log file

					unfollow_count += 1

					following_button = liElements[element].find_element_by_xpath(".//div/div/span/button")
					following_button.click()
					print("Now unfollowing: " + elementUsername.text)
					print("Unfollow count: " + str(unfollow_count) + "\n")
					t.eraseUsernameFromLog(elementUsername.text, followedDate, self.username)
					
					sleepy += 1

			else:

				# No data from that user, the person was not followed by the script and will be unfollowed and erased from the log file

				unfollow_count += 1

				following_button = liElements[element].find_element_by_xpath(".//div/div/span/button")
				following_button.click()
				print("Now unfollowing: " + elementUsername.text)
				print("Unfollow count: " + str(unfollow_count) + "\n")
				t.eraseUsernameFromLog(elementUsername.text, followedDate, self.username)

				sleepy += 1
			
			element += 1
			
			# If the sleep count variable match the sleep interval and the unfollow count hasn't reached his goal, sleep for x minutes

			if sleepy % sleepInterval == 0:

				if sleepy != 0:

					if unfollow_count < self.amount:

						print("Sleeping for " + str(sleepTime) + " minutes\n")
						sleepy = 0
						sleep(sleepTime * 60)

			# Check for bugs

			if unfollow_count > amountCheck:
				break
			
			sleep(sleepTime - 2)


		log.close()

	def follow_user_followers(self, amount, user, delay):

		self.amount = amount
		self.user = user
		self.delay = delay

		print("User: " + self.username)
		print("Follow from: " + self.user)

		# Go to user's profile


		self.browser.get("https://www.instagram.com/" + user + "/")


		# Search for 'followers' button and click it
		

		followers_element = self.browser.find_element_by_xpath("//article/header/section/ul/li[2]")
		followers_element.click()

		#followersAmount = followers_element.find_element_by_xpath(".//a/span").text	
		#followersAmount = int(followersAmount)


		# Load as much users as the amount variable says


		self.loadUsers(self.amount, 2, user, "follow", 5000)

		ulElement = self.browser.find_element_by_xpath("//div/div/div/div/div/ul/div")
		sleep(2)
		liElements = ulElement.find_elements_by_tag_name("li")
		liCount = len(liElements)
	

		# Iterate through the list of users


		log = open("C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/Logs/" + self.username + ".txt", "a")
		today = date.today()
		follow_count = 0
		element = 0

		print("\n --- Starting follow session --- \n")

		while follow_count < self.amount:

			elementUsername = liElements[element].find_element_by_xpath(".//div/div/div/div/a")
			sleep(2)

			t = TestLogs()

			followedDate = t.getDateByUsername(elementUsername.text, self.username)
			
			if followedDate != '':

				print("El usuario " + elementUsername.text + " ya fue seguido en la fecha " + followedDate + "\n")
				element += 1
				continue

			else:

				# No hay data del usuario

				follow_button = liElements[element].find_element_by_xpath(".//div/div/span/button")

				# Si no se lo esta siguiendo

				if follow_button.text != "Following" and follow_button.text != "Requested":

					follow_button.click()

					sleep(5)

					if follow_button.text == "Follow":

						print("Error\n")

					else:

						follow_count += 1
						print("Now following: " + elementUsername.text)
						print("Follow count: " + str(follow_count) + "\n")
						log.write(elementUsername.text + ' - ' + str(today) +'\n')

				else:

					print("Already following user: " + elementUsername.text + "\n")

			
			element += 1
			sleep(delay - 7)

		log.close()

	def like_by_hashtag(self, amount, hashtag, delay):

		self.amount = amount
		self.hashtag = hashtag
		self.delay = delay

		like_count = 0

		self.browser.get("https://www.instagram.com/explore/tags/" + hashtag)

		first_post = self.browser.find_element_by_xpath("//article/div/div/div/div/a/div")
		first_post.click()
		sleep(2)

		while like_count < amount:

			like_hearth = self.browser.find_element_by_xpath("//article/div/section/a/span")

			if like_hearth.text == "Like":

				like_hearth.click()
				like_count += 1
				print("Like count: " + str(like_count))
			

			next_arrow = self.browser.find_element_by_xpath("//body/div/div/div/div/div/a[text()='Next']")
			next_arrow.click()

			sleep(delay)
	
	def like_by_hashtags(self, hashtags, amount, delay):
		
		self.hashtags = hashtags
		self.amount = 15
		self.delay = 5

		print(hashtags)
		print(amount)
		print(delay)

		amountPerHashtag = amount / len(hashtags)
		amountPerHashtag = int(amountPerHashtag)
		
		like_count = 0
		like_count_per_tag = 0
		
		print(" --- Starting liking session --- ")

		for hashtag in hashtags:
			
			print("HASHTAG: " + hashtag)

			self.browser.get("https://www.instagram.com/explore/tags/" + hashtag)

			first_post = self.browser.find_element_by_xpath("//article/div/div/div/div/a/div")
			first_post.click()
			sleep(2)

			while like_count_per_tag < amountPerHashtag:

				# Check if next post has been erased. If so, reload the page and continue liking

				checkError = self.browser.find_element_by_xpath("//body")

				if checkError.get_attribute("class") == " p-error dialog-404":

					print("Post erased. Reloading page...")
					self.browser.get("https://www.instagram.com/explore/tags/" + hashtag)

					first_post = self.browser.find_element_by_xpath("//article/div/div/div/div/a/div")
					first_post.click()
					sleep(2)

				like_hearth = self.browser.find_element_by_xpath("//article/div/section/a/span")

				if like_hearth.text == "Like":

					like_hearth.click()
					like_count += 1
					like_count_per_tag += 1
					print("Like count: " + str(like_count))
			

				next_arrow = self.browser.find_element_by_xpath("//body/div/div/div/div/div/a[text()='Next']")
				next_arrow.click()

				sleep(delay)

			like_count_per_tag = 0

	def end(self):

		# Closes the current session

		self.browser.delete_all_cookies()
		self.browser.quit()
		print("Session finished")
		input("Press any key to exit")