import urllib.request
import uuid
from datetime import date
from random import randint
from time import sleep

from Scripts.testLogs import TestLogs
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


class InstaBot(object):

    def __init__(self, username, password, wait_time=0.1):

        self.username = username
        self.password = password
        self.wait_time = wait_time
        self.browser = None
        self.body_elem = None
        self.headless = None
        self.load_images = None

    def start(self, headless=False, load_images=True):

        self.headless = headless
        self.load_images = load_images

        # Starting a new browser session

        print("███████████████████████████████████████████████████████████████████████████████")
        print("█                                 STARTING UP!                                █")
        print("███████████████████████████████████████████████████████████████████████████████")

        options = Options()

        if self.headless and self.load_images:

            options.add_argument("--headless")  # Runs Chrome in headless mode.
            options.add_argument('--no-sandbox')  # Bypass OS security model
            options.add_argument('--disable-gpu')  # applicable to windows os only
            options.add_argument('start-maximized')  # Maximize chrome
            options.add_argument('disable-infobars')
            options.add_argument("--disable-extensions")

            self.browser = webdriver.Chrome(chrome_options=options,
                                            executable_path=r'C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/ChromeDriver/chromedriver.exe')

        elif self.headless and not self.load_images:

            options.add_argument("--headless")  # Runs Chrome in headless mode.
            options.add_argument('--no-sandbox')  # Bypass OS security model
            options.add_argument('--disable-gpu')  # Applicable to windows os only
            options.add_argument('start-maximized')  # Maximize chrome
            options.add_argument('disable-infobars')
            options.add_argument("--disable-extensions")

            self.browser = webdriver.Chrome(chrome_options=options,
                                            executable_path=r'C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/ChromeDriver/chromedriver.exe')

        elif not self.headless and self.load_images:

            self.browser = webdriver.Chrome(
                'C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/ChromeDriver/chromedriver.exe')
            self.browser.maximize_window()

        elif not self.headless and not self.load_images:

            self.browser = webdriver.Chrome(
                'C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/ChromeDriver/chromedriver.exe')
            self.browser.maximize_window()

        '''

		# With images

		#self.browser = webdriver.Chrome('C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/ChromeDriver/chromedriver.exe')
		#self.browser.maximize_window()

		# Without images

		 #chromeOptions = webdriver.ChromeOptions()
		 #prefs = {"profile.managed_default_content_settings.images":2}
		 #chromeOptions.add_experimental_option("prefs",prefs)
		 #self.browser = webdriver.Chrome(chrome_options=chromeOptions)
		 #self.browser.maximize_window()

		self.browser = webdriver.Firefox(executable_path='C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/ChromeDriver/geckodriver.exe')
		'''

    def mobile_view(self):

        mobile_emulation = {

            "deviceMetrics": {"width": 360, "height": 620, "pixelRatio": 3.0},

            "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19"}

        chrome_options = Options()

        chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)

        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        self.browser.maximize_window()

    def login(self):

        # Navigating to a webpage

        self.browser.get('https://www.instagram.com')

        # Find log in

        sleep(5)

        login_element = self.browser.find_element_by_xpath('//article/div/div/div/span/button')

        # Click it

        login_element.click()
        sleep(2)

        # Find forms and enter data

        input_username = self.browser.find_element_by_name('username')
        input_password = self.browser.find_element_by_name('password')

        ActionChains(self.browser) \
            .move_to_element(input_username).click() \
            .send_keys(self.username) \
            .move_to_element(input_password).click() \
            .send_keys(self.password) \
            .perform()

        # Find log in button and click it

        login_button = self.browser.find_element_by_xpath('//form/span/button[text()="Log in"]')

        ActionChains(self.browser) \
            .move_to_element(login_button) \
            .click().perform()

        sleep(5)

        self.browser.implicitly_wait(self.wait_time)

    def load_users(self, amount, multiplier, user, action, maximum):

        self.amount = amount
        self.multiplier = multiplier
        self.user = user
        self.action = action
        self.maximum = maximum
        self.following_element = None
        self.followers_element = None

        self.body_elem = self.browser.find_element_by_tag_name("body")

        print("Loading users...")

        try:

            ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")

        except NoSuchElementException:

            sleep(1)
            try:

                ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")

            except NoSuchElementException:

                print("There was a problem loading users")

        ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")
        sleep(2)
        li_elements = ul_element.find_elements_by_tag_name("li")
        li_count = len(li_elements)

        error_count = 0
        last_count = li_count

        ul_element.click()

        # Start the load loop

        for i in range(100):

            # Print current stats

            print("Amount of elements:", li_count)
            print("Desired amount:", self.amount * self.multiplier)
            print()

            # If li_count is staying the same

            if li_count == last_count:

                error_count += 1

                # If it occurred three times in a row

                if error_count >= 3:

                    error_count = 0

                    # Reload the page

                    print("Error. Restarting process...")
                    self.browser.get('https://www.instagram.com/' + self.user + '/')

                    if action == "follow":

                        self.followers_element = self.browser.find_element_by_xpath("//main/div/ul/li[2]/a")
                        self.followers_element.click()

                        sleep(2)

                        ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")
                        sleep(2)
                        li_elements = ul_element.find_elements_by_tag_name("li")
                        li_count = len(li_elements)

                        ul_element.click()

                        self.body_elem = self.browser.find_element_by_tag_name("body")

                    elif action == "unfollow":

                        self.following_element = self.browser.find_element_by_xpath("//main/div/ul/li[3]/a")
                        self.following_element.click()

                        sleep(2)

                        ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")
                        sleep(2)
                        li_elements = ul_element.find_elements_by_tag_name("li")
                        li_count = len(li_elements)

                        ul_element.click()

                        self.body_elem = self.browser.find_element_by_tag_name("body")

            # Set the last_count variable equal to the latest value of li_count before its re-assignment

            last_count = li_count

            # If user amount isn't enough yet

            if li_count < (self.amount * self.multiplier) and li_count < maximum:

                for j in range(5):
                    self.body_elem.send_keys(Keys.END)
                    sleep(2)

                ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")
                sleep(2)
                li_elements = ul_element.find_elements_by_tag_name("li")
                li_count = len(li_elements)

            else:

                print("Desired amount of users complete")
                self.body_elem.send_keys(Keys.HOME)
                break

    def unfollow_users(self, amount, sleep_time, sleep_interval, only_script_followed):

        self.amount = amount
        self.sleep_time = sleep_time
        self.sleep_interval = sleep_interval
        self.only_script_followed = only_script_followed
        self.following_element = None

        amount_check = self.amount

        t = TestLogs()

        # Go to user's profile

        self.browser.get("https://www.instagram.com/" + self.username + "/")

        # Search for 'following' button and click it

        following_element = self.browser.find_element_by_xpath("//main/div/ul/li[3]/a")
        following_element.click()

        following_amount = following_element.find_element_by_xpath(".//span").text
        following_amount = int(t.removeCharFromString(following_amount, ','))
        print(following_amount)

        # Load as much users as the amount variable says

        self.load_users(self.amount, 3, self.username, "unfollow", following_amount)

        ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")
        sleep(2)
        li_elements = ul_element.find_elements_by_tag_name("li")
        li_count = len(li_elements)

        # Iterate through the list of users and open the log file to check and update it after each loop

        log = open("C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/Logs/" + self.username + ".txt",
                   "r")
        today = date.today()
        unfollow_count = 0
        element = 0
        sleepy = 0

        print("\n --- Starting unfollow session --- \n")

        while unfollow_count < self.amount:

            # If there is no more elements and the loop hasn't concluded yet, load new ones and keep going

            if element == (len(li_elements) - 1):

                if (len(li_elements) + 100) < following_amount:

                    self.load_users(len(li_elements) + 100, 1, self.user, "unfollow", following_amount)

                elif len(li_elements) <= following_amount:

                    self.load_users(following_amount, 1, self.user, "unfollow", following_amount)

                ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")
                sleep(2)
                li_elements = ul_element.find_elements_by_tag_name("li")
                li_count = len(li_elements)

            # Find the current element username and check the log to see when was he followed

            try:

                element_username = li_elements[element].find_element_by_xpath(".//div/div/div/div/a")

            except IndexError:

                print("There are no more users")
                break

            sleep(2)

            followed_date = t.getDateByUsername(element_username.text, self.username)

            # If we have data about that user (has been followed using this script)

            if followed_date != '':

                print(followed_date)
                days_difference = t.compareDateWithToday(followed_date)

                # Compare the date he was followed with today's date, if the difference is bigger than two,
                # the person will be unfollowed

                if days_difference < 2:

                    print("Cant unfollow user " + element_username.text + " yet\n")
                    element += 1
                    continue

                else:

                    # More than two days, user will be unfollowed and erased from the log file

                    following_button = li_elements[element].find_element_by_xpath(".//div/div[2]/button")
                    following_button.click()

                    try:

                        unfollow_button_window = self.browser.find_element_by_xpath(
                            "//body/div/div/div/div/div[3]/button[1]")

                        unfollow_button_window.click()

                    except NoSuchElementException:

                        pass

                    unfollow_count += 1

                    print("Now unfollowing: " + element_username.text)
                    print("Unfollow count: " + str(unfollow_count) + "\n")
                    t.eraseUsernameFromLog(element_username.text, followed_date, self.username)

                    sleepy += 1

            elif self.only_script_followed == False:

                # No data from that user, the person was not followed by the script and will be unfollowed and erased from the log file

                following_button = li_elements[element].find_element_by_xpath(".//div/div[2]/button")
                following_button.click()

                try:

                    unfollow_button_window = self.browser.find_element_by_xpath(
                        "//body/div/div/div/div/div[3]/button[1]")

                    unfollow_button_window.click()

                except NoSuchElementException:

                    pass

                unfollow_count += 1

                print("Now unfollowing: " + element_username.text)
                print("Unfollow count: " + str(unfollow_count) + "\n")
                t.eraseUsernameFromLog(element_username.text, followed_date, self.username)

                sleepy += 1

            else:

                print("Cannot unfollow " + element_username.text + " since the user was followed organically\n")
                element += 1
                continue

            element += 1

            # If the sleep count variable match the sleep interval and the unfollow count hasn't reached his goal, sleep for x minutes

            if sleepy % sleep_interval == 0:

                if sleepy != 0:

                    if unfollow_count < self.amount:
                        print("Sleeping for " + str(sleep_time) + " minutes\n")
                        sleepy = 0
                        sleep(sleep_time * 60)

            # Check for bugs

            if unfollow_count > amount_check:
                break

            sleep(sleep_time - 2)

        log.close()

    def follow_user_followers(self, amount, user, delay, only_with_profile_pic=True):

        self.amount = amount
        self.user = user
        self.delay = delay
        self.only_with_profile_pic = only_with_profile_pic

        default_profile_pic = "https://scontent-nrt1-1.cdninstagram.com/vp/da767c5fe26fe6a419aad4f352e7f45a/5BE3737A/t51.2885-19/11906329_960233084022564_1448528159_a.jpg"
        default_profile_pic2 = "https://instagram.fhel6-1.fna.fbcdn.net/vp/da767c5fe26fe6a419aad4f352e7f45a/5BE3737A/t51.2885-19/11906329_960233084022564_1448528159_a.jpg"

        print("User: " + self.username)
        print("Follow from: " + self.user)

        # Go to user's profile

        self.browser.get("https://www.instagram.com/" + user + "/")

        # Search for 'followers' button and click it

        followers_element = self.browser.find_element_by_xpath("//main/div/ul/li[2]/a")
        followers_element.click()

        # followersAmount = followers_element.find_element_by_xpath(".//a/span").text
        # followersAmount = int(followersAmount)

        # Load as much users as the amount variable says

        self.load_users(self.amount, 2, user, "follow", 5000)

        ul_element = self.browser.find_element_by_xpath("//body/span/section/main/ul/div")
        sleep(2)
        li_elements = ul_element.find_elements_by_tag_name("li")
        li_count = len(li_elements)

        # Iterate through the list of users

        log = open("C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/Logs/" + self.username + ".txt",
                   "a")
        today = date.today()
        follow_count = 0
        element = 0

        print("\n --- Starting follow session --- \n")

        while follow_count < self.amount:

            element_username = li_elements[element].find_element_by_xpath(".//div/div/div/div/a")
            element_image = li_elements[element].find_element_by_xpath(".//div/div/a/img")
            element_image = element_image.get_attribute("src")
            sleep(2)

            t = TestLogs()

            followed_date = t.getDateByUsername(element_username.text, self.username)

            if followed_date != '':

                print("The user " + element_username.text + " was already followed on " + followed_date + "\n")
                element += 1
                continue

            else:

                # No data from that user

                follow_button = li_elements[element].find_element_by_xpath(".//div/div[2]/button")

                # If we are not following the user

                if follow_button.text != "Following" and follow_button.text != "Requested":

                    if element_image == default_profile_pic or element_image == default_profile_pic2:

                        if not self.only_with_profile_pic:

                            follow_button.click()

                            sleep(5)

                            if follow_button.text == "Follow":

                                print("Error\n")

                            else:

                                follow_count += 1
                                print("Now following: " + element_username.text)
                                print("Follow count: " + str(follow_count) + "\n")
                                log.write(element_username.text + ' - ' + str(today) + '\n')

                        else:

                            print("Can't follow users with no profile picture")
                            element += 1
                            continue

                    else:

                        follow_button.click()

                        sleep(5)

                        if follow_button.text == "Follow":

                            print("Error\n")

                        else:

                            follow_count += 1
                            print("Now following: " + element_username.text)
                            print("Follow count: " + str(follow_count) + "\n")
                            log.write(element_username.text + ' - ' + str(today) + '\n')

                else:

                    print("Already following user: " + element_username.text + "\n")
                    element += 1
                    continue

            element += 1
            sleep(delay - 7)

        log.close()

    def like_by_hashtags(self, hashtags, amount, delay):

        self.hashtags = hashtags
        self.amount = 15
        self.delay = 5

        print(hashtags)
        print(amount)
        print(delay)

        amount_per_hashtag = amount / len(hashtags)
        amount_per_hashtag = int(amount_per_hashtag)

        like_count = 0
        like_count_per_tag = 0

        print(" --- Starting liking session --- ")

        for hashtag in hashtags:

            print("HASHTAG: " + hashtag)

            self.browser.get("https://www.instagram.com/explore/tags/" + hashtag)

            first_post = self.browser.find_element_by_xpath("//article/div[1]/div/div/div/div/a/div")
            first_post.click()
            sleep(2)

            while like_count_per_tag < amount_per_hashtag:

                # Check if next post has been erased. If so, reload the page and continue liking

                check_error = self.browser.find_element_by_xpath("//body")

                if check_error.get_attribute("class") == " p-error dialog-404":
                    print("Post erased. Reloading page...")
                    self.browser.get("https://www.instagram.com/explore/tags/" + hashtag)

                    first_post = self.browser.find_element_by_xpath("//article/div/div/div/div/a/div")
                    first_post.click()
                    sleep(2)

                like_heart = self.browser.find_element_by_xpath("//article/div[2]/section/span/button/span")
                like_heart_text = like_heart.get_attribute("aria-label")

                if like_heart_text == "Like":
                    like_heart.click()
                    like_count += 1
                    like_count_per_tag += 1
                    print("Like count: " + str(like_count))

                try:

                    next_arrow = self.browser.find_element_by_xpath("//body/div/div/div/div/div/a[text()='Next']")
                    next_arrow.click()

                except NoSuchElementException:

                    print("There was a problem loading posts in the hashtag")
                    break

                sleep(delay)

            like_count_per_tag = 0

    def rounds(self, accounts, comments, delay):

        self.accounts = accounts
        self.comments = comments
        self.delay = delay

        print("--- Starting Round Session ---")

        for account in accounts:

            self.browser.get("https://www.instagram.com/" + account)

            sleep(5)

            most_recent = self.browser.find_element_by_xpath("//article/div/div/div/div/a/div")
            most_recent.click()

            sleep(2)

            like_heart = self.browser.find_element_by_xpath("//article/div[2]/section/span/button/span")
            like_heart_text = like_heart.get_attribute("aria-label")

            if like_heart_text == "Like":
                like_heart.click()
                print("Liked " + account + " most recent post!")

            sleep(2)

            comment_box = self.browser.find_element_by_xpath("//article/div[2]/section[3]/div/form/textarea")

            sleep(5)

            random_comment = randint(0, len(comments) - 1)

            ActionChains(self.browser) \
                .move_to_element(comment_box).click() \
                .send_keys(comments[random_comment]) \
                .send_keys(Keys.ENTER) \
                .perform()

            print("Commented " + account + " most recent post!\n")

            sleep(delay)

    def dollar_eighty(self, hashtags, comments, automated, delay):

        self.hashtags = hashtags
        self.comments = comments
        self.automated = automated
        self.delay = delay

        self.count = 0

        # For each hashtag in the given list

        for hashtag in hashtags:

            # Go to the hashtag page

            self.browser.get("http://www.instagram.com/explore/tags/" + hashtag)

            # Get the first post and click it

            first_post = self.browser.find_element_by_xpath("//article/div[1]/div/div/div/div/a/div")
            first_post.click()

            # While it hasn't commented on the top 9 posts of the current hashtag

            while self.count < 9:

                sleep(2)

                # Get the comments list of the post

                comments_ul = self.browser.find_element_by_xpath("//article/div[2]/div/ul")
                comment_list = comments_ul.find_elements_by_tag_name("li")

                # Load hidden comments

                while comment_list[0].get_attribute("class") != comment_list[1].get_attribute("class"):
                    load_comments = comment_list[1].find_element_by_xpath(".//button")
                    self.browser.execute_script("arguments[0].click();", load_comments)

                    comments_ul = self.browser.find_element_by_xpath("//article/div[2]/div/ul")
                    comment_list = comments_ul.find_elements_by_tag_name("li")

                # Get the full list of comments

                comments_ul = self.browser.find_element_by_xpath("//article/div[2]/div/ul")
                comment_list = comments_ul.find_elements_by_tag_name("li")

                # Check if we commented this post already

                already_commented = False

                for comment in comment_list:

                    comment_link = comment.find_element_by_xpath(".//div/div/div/a")
                    user = comment_link.get_attribute("title")

                    if user == self.username:
                        already_commented = True
                        break

                if already_commented:

                    print("Already commented this post")
                    self.count += 1

                else:

                    # If we haven't comment this one yet, comment on it

                    comment_box = self.browser.find_element_by_xpath("//article/div[2]/section[3]/div/form/textarea")

                    sleep(5)

                    if automated:

                        comm = randint(0, len(comments) - 1)

                    else:

                        print("Insert comment:\n")
                        comm = input()

                    ActionChains(self.browser) \
                        .move_to_element(comment_box).click() \
                        .send_keys(comments[comm]) \
                        .send_keys(Keys.ENTER) \
                        .perform()

                    self.count += 1

                    print("Commented a top post on " + hashtag + "\n")
                    print("Comment count on current hashtag: " + str(self.count) + "\n")

                    sleep(2)

                # Go to the next post on the hashtag

                try:

                    next_arrow = self.browser.find_element_by_xpath("//body/div/div/div/div/div/a[text()='Next']")
                    next_arrow.click()

                except NoSuchElementException:

                    print(
                        "There are no more posts. The script will go to the next hashtag (if this isn't the last one)")

                # sleep(2)
                sleep(self.delay - 7)

            self.count = 0

    def like_and_comment(self, accounts, comments, amount, delay):

        self.accounts = accounts
        self.comments = comments
        self.amount = amount
        self.delay = delay

        self.count = 0

        for account in self.accounts:

            self.browser.get("https://www.instagram.com/" + account)

            sleep(5)

            most_recent = self.browser.find_element_by_xpath("//article/div/div/div/div/a/div")
            most_recent.click()

            while self.count < amount:

                sleep(2)

                like_heart = self.browser.find_element_by_xpath("//article/div[2]/section/span/button/span")
                like_heart_text = like_heart.get_attribute("aria-label")

                if like_heart_text == "Like":
                    like_heart.click()
                    print("Liked " + account + " most recent post!")

                sleep(2)

                comment_box = self.browser.find_element_by_xpath("//article/div[2]/section[3]/div/form/textarea")

                sleep(5)

                random_comment = randint(0, len(comments) - 1)

                ActionChains(self.browser) \
                    .move_to_element(comment_box).click() \
                    .send_keys(comments[random_comment]) \
                    .send_keys(Keys.ENTER) \
                    .perform()

                print("Here i'd comment")
                print("Commented " + account + " most recent post!\n")

                self.count += 1

                try:

                    next_arrow = self.browser.find_element_by_xpath("//body/div/div/div/div/div/a[text()='Next']")
                    next_arrow.click()

                except NoSuchElementException:

                    print("There are no more posts. The script will go to the next account (if this isn't the last)")

                # sleep(2)
                sleep(self.delay - 7)

            self.count = 0

    def save_content_by_accounts(self, accounts, amount, only_popular=False, only_videos=False, only_images=False,
                                 only_first_on_multi=False):

        self.accounts = accounts
        self.amount = amount
        self.only_popular = only_popular
        self.only_videos = only_videos
        self.only_images = only_images
        self.only_first_on_multi = only_first_on_multi

        saved_amount = 0
        image = False
        multi_image = False
        video = False

        t = TestLogs()

        for account in accounts:

            if saved_amount == self.amount:
                break

            self.browser.get("https://www.instagram.com/" + account + "/")

            sleep(3)

            self.max_posts = self.browser.find_element_by_xpath("//main/div/header/section/ul/li/span/span")
            self.max_posts = int(t.removeCharFromString(self.max_posts.text, ","))

            if self.amount > self.max_posts:
                self.amount = self.max_posts

            post_section = self.browser.find_element_by_xpath("//main/div/div[3]/article/div/div")
            sleep(2)
            div_elements = post_section.find_elements_by_xpath("*")
            div_count = len(div_elements) * 3

            if self.only_popular:

                likes_amount_combined = 0
                amount_checked = 0
                amount_to_check = self.max_posts
                average_likes = 0

                if amount_to_check > 100:
                    amount_to_check = 100

                posts_div = div_elements[0]

                while amount_checked < amount_to_check:

                    posts = posts_div.find_elements_by_xpath("*")

                    for post in posts:

                        hover = ActionChains(self.browser).move_to_element(post)
                        hover.perform()

                        try:

                            likes_element = post.find_element_by_xpath(".//a/div[2]/ul/li/span")
                            likes_amount = likes_element.text
                            likes_amount = t.removeCharFromString(likes_amount, ",")
                            likes_amount = t.fixNumsWithKM(likes_amount)
                            likes_amount_combined += float(likes_amount)
                            amount_checked += 1

                        except NoSuchElementException:

                            try:

                                likes_element = post.find_element_by_xpath(".//a/div[3]/ul/li/span")
                                likes_amount = likes_element.text
                                likes_amount = t.removeCharFromString(likes_amount, ",")
                                likes_amount = t.fixNumsWithKM(likes_amount)
                                likes_amount_combined += float(likes_amount)
                                amount_checked += 1

                            except NoSuchElementException:

                                print("Hover failed. Hidden element is still hidden")

                    div_elements = post_section.find_elements_by_xpath("*")

                    for i in range(len(div_elements)):

                        post_div_url_comparison = (posts_div.find_element_by_xpath(".//div/a")).get_attribute("href")
                        currentDivURLComparison = (div_elements[i].find_element_by_xpath(".//div/a")).get_attribute(
                            "href")

                        if post_div_url_comparison == currentDivURLComparison:

                            try:

                                posts_div = div_elements[i + 1]
                                break

                            except IndexError:

                                print("There are no more posts")
                                amount_checked = amount_to_check
                                break

                print("Amount: " + str(amount_checked))
                print("Total likes: " + str(int(likes_amount_combined)))
                print("Average likes: " + str(int(likes_amount_combined / amount_checked)))

                average_likes = int(likes_amount_combined / amount_checked)

            # Starting saving content

            sleep(3)

            self.body_elem = self.browser.find_element_by_tag_name("body")
            self.body_elem.send_keys(Keys.HOME)

            sleep(3)

            post_section = self.browser.find_element_by_xpath("//main/div/div[3]/article/div/div")
            post_link = post_section.find_element_by_xpath(".//div/div/a/div")

            self.browser.execute_script("arguments[0].click();", post_link)
            sleep(1)

            while saved_amount < self.amount:

                divide_media_element = self.browser.find_element_by_xpath(
                    "//div/div/div/div/article/div/div/div/div[1]")

                childs = divide_media_element.find_elements_by_xpath("*")
                child_count = len(childs)

                if child_count == 3:

                    print("Video")
                    image = False
                    multi_image = False
                    video = True

                else:

                    try:

                        img_tag = divide_media_element.find_element_by_xpath(".//div/div/img")

                        print("Multi-Image")
                        video = False
                        image = False
                        multi_image = True

                    except NoSuchElementException:

                        print("Image")
                        video = False
                        multi_image = False
                        image = True

                if self.only_popular:

                    if video and not only_images:

                        # viewsAndLikesElement = self.browser.find_element_by_xpath("//div/div/div[2]/div/article/div[2]/section[2]/div")
                        # views_button_span = viewsAndLikesElement.find_element_by_xpath(".//span")

                        # self.browser.execute_script("arguments[0].click();", views_button_span)

                        # sleep(1)

                        try:

                            likes_span = self.browser.find_element_by_xpath(
                                "//div/div/div[2]/div/article/div[2]/section[2]/div/div/div[4]/span")
                            video_likes = int(t.removeCharFromString(likes_span.text, ","))
                            print(likes_span.text)

                            sleep(5)

                            print("Video likes: " + str(video_likes))
                            print("Average likes: " + str(average_likes))

                            if video_likes >= average_likes:
                                print("Its popular")
                                video_tag = divide_media_element.find_element_by_xpath(".//div/div/video")
                                url = video_tag.get_attribute("src")

                                urllib.request.urlretrieve(url, './ContentTest/Videos/' + str(uuid.uuid4()) + '.mp4')

                                saved_amount += 1

                        except NoSuchElementException:

                            print("In first except")
                            views_button_span = self.browser.find_element_by_xpath(
                                "//div/div/div[2]/div/article/div[2]/section[2]/div/span")

                            self.browser.execute_script("arguments[0].click();", views_button_span)

                            sleep(1)

                            try:

                                likes_span = self.browser.find_element_by_xpath(
                                    "//div/div/div[2]/div/article/div[2]/section[2]/div/div/div[4]/span")
                                print(likes_span.text)

                                sleep(5)

                                video_likes = int(t.removeCharFromString(likes_span.text, ","))
                                print("Video likes: " + str(video_likes))
                                print("Average likes: " + str(average_likes))

                                if video_likes >= average_likes:
                                    print("Its popular")
                                    video_tag = divide_media_element.find_element_by_xpath(".//div/div/video")
                                    url = video_tag.get_attribute("src")

                                    urllib.request.urlretrieve(url,
                                                               './ContentTest/Videos/' + str(uuid.uuid4()) + '.mp4')

                                    saved_amount += 1

                            except NoSuchElementException:

                                print("Something went wrong getting the video likes amount")

                        sleep(1)

                    if image and not only_videos:

                        likes_span = self.browser.find_element_by_xpath(
                            "//div/div/div[2]/div/article/div[2]/section[2]/div/a/span")
                        image_likes = int(t.removeCharFromString(likes_span.text, ","))

                        print("Image likes: " + str(image_likes))
                        print("Average likes: " + str(average_likes))

                        if image_likes >= average_likes:
                            print("Its popular")
                            img_tag = divide_media_element.find_element_by_tag_name("img")
                            url = img_tag.get_attribute("src")

                            urllib.request.urlretrieve(url, './ContentTest/Images/' + str(uuid.uuid4()) + '.jpg')

                            saved_amount += 1

                    if multi_image and not only_videos:

                        likes_span = self.browser.find_element_by_xpath(
                            "//div/div/div[2]/div/article/div[2]/section[2]/div/a/span")
                        image_likes = int(t.removeCharFromString(likes_span.text, ","))

                        print("Image likes: " + str(image_likes))
                        print("Average likes: " + str(average_likes))

                        if image_likes >= average_likes:

                            print("Its popular")

                            multi_ul = divide_media_element.find_element_by_xpath(".//div[2]/div/div/div/ul")
                            multi_posts = multi_ul.find_elements_by_tag_name("li")

                            for post in multi_posts:

                                img_tag = post.find_element_by_xpath(".//div/div/div/div/img")

                                url = img_tag.get_attribute("src")

                                urllib.request.urlretrieve(url, './ContentTest/Images/' + "Multi-" + str(
                                    uuid.uuid4()) + '.jpg')

                                saved_amount += 1

                                if post != multi_posts[(len(multi_posts)) - 1]:

                                    if post == multi_posts[0]:

                                        next_photo = self.browser.find_element_by_xpath(
                                            "//div[3]/div/div[2]/div/article/div/div/div/div/div[2]/button/div")
                                        next_photo.click()

                                    else:

                                        next_photo = self.browser.find_element_by_xpath(
                                            "//div[3]/div/div[2]/div/article/div/div/div/div/div[2]/button[2]/div")
                                        next_photo.click()

                                    sleep(1)

                else:

                    if image and not only_videos:
                        img_tag = divide_media_element.find_element_by_tag_name("img")
                        url = img_tag.get_attribute("src")

                        urllib.request.urlretrieve(url, './ContentTest/Images/' + str(uuid.uuid4()) + '.jpg')

                        saved_amount += 1

                    if video and not only_images:
                        video_tag = divide_media_element.find_element_by_xpath(".//div/div/video")
                        url = video_tag.get_attribute("src")

                        urllib.request.urlretrieve(url, './ContentTest/Videos/' + str(uuid.uuid4()) + '.mp4')

                        saved_amount += 1

                    if multi_image and not only_videos:

                        next_photo = self.browser.find_element_by_xpath("//div/div/div/div/article/div/div/div/a")
                        photos_amount_dots = self.browser.find_element_by_xpath(
                            "//div/div/div/div/article/div/div/div/div[2]")

                        photos_amount = len(photos_amount_dots.find_elements_by_xpath("*"))

                        if only_first_on_multi:
                            photos_amount = 1

                        for i in range(photos_amount):

                            divide_media_element = self.browser.find_element_by_xpath(
                                "//div/div/div/div/article/div/div/div/div[1]")
                            img_tag = divide_media_element.find_element_by_xpath(".//div/div/img")
                            url = img_tag.get_attribute("src")

                            urllib.request.urlretrieve(url, './ContentTest/Images/' + "Multi-" + str(
                                uuid.uuid4()) + '.jpg')
                            saved_amount += 1

                            if i < photos_amount:

                                if i == 0:

                                    next_photo = self.browser.find_element_by_xpath(
                                        "//div/div/div/div/article/div/div/div/a")
                                    next_photo.click()

                                elif i != (photos_amount - 1):

                                    next_photo = self.browser.find_element_by_xpath(
                                        "//div/div/div/div/article/div/div/div/a[2]")
                                    next_photo.click()

                                sleep(1)

                print("Saved amount: " + str(saved_amount))
                sleep(1)

                if saved_amount < self.amount:

                    try:

                        next_post = self.browser.find_element_by_xpath("//body/div/div/div/div/div/a[text()='Next']")

                        self.browser.execute_script("arguments[0].click();", next_post)

                        sleep(1)

                    except NoSuchElementException:

                        print("The user does not have any more posts")
                        saved_amount = self.amount

    def post(self, path, caption):

        self.path = path
        self.caption = caption

        not_now = self.browser.find_element_by_xpath("//body/span/div/div[2]/a[2]")
        not_now.click()

        post_button = self.browser.find_element_by_xpath("//body/span/section/nav[2]/div/div/div[2]/div/div/div[3]")
        post_button.send_keys(self.path)

        sleep(100)

    def end(self):

        # Closes the current session

        self.browser.delete_all_cookies()
        self.browser.quit()
        print("Session finished")
        input("Press any key to exit")
