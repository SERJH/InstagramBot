from datetime import date

class TestLogs(object):

	def __init__(self):
		pass

	def getDateByUsername(self, usernameToFind, logName):

		self.usernameToFind = usernameToFind
		self.logName = logName

		log = open("C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/Logs/" + logName + ".txt", "r")

		separadorSuperado = False
		contador = 0
		strUser = ''
		strFecha = ''

		strYear = ''
		strMonth = ''
		srtDay = ''

		for line in log:

			for i in range(len(line)):
	
				if not separadorSuperado:

					if line[i] == ' ' or line[i] == '-':

						contador += 1

						if contador == 3:

							separadorSuperado = True
							continue

						continue
				
					else:

						strUser += line[i]

				elif i != (len(line) - 1):

					strFecha += line[i]

			# Si encuentra al usuario

			if usernameToFind == strUser:

				log.close()
				return strFecha

			# De lo contrario resetea variables y sigue buscando

			strUser = ''
			strFecha = ''
	
			separadorSuperado = False
			contador = 0

		log.close()

		# Si se acaba el archivo y aún no lo encontró devuelve string vacia

		return ''

	def compareDateWithToday(self, dateToCompare):

		self.dateToCompare = dateToCompare

		strYear = ''
		strMonth = ''
		srtDay = ''

		for i in range(len(dateToCompare)):

			if i <= 3:
		
				strYear += dateToCompare[i]

			elif i == 5 or i == 6:

				strMonth += dateToCompare[i]

			elif i == 8 or i == 9:

				srtDay += dateToCompare[i]

		dateToCompareObj = date(int(strYear), int(strMonth), int(srtDay))
		dateToday = date.today()

		return abs(dateToday - dateToCompareObj).days

	def eraseUsernameFromLog(self, usernameToFind, usernameFollowDate, logName):

		self.usernameToFind = usernameToFind
		self.logName = logName

		log = open("C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/Logs/" + logName + ".txt", "r")
		lines = log.readlines()
		log.close()

		lineToDelete = usernameToFind + " - " + usernameFollowDate + "\n"

		log = open("C:/Users/SERJH/Desktop/Programacion/Bots/Instagram Bot/InstaBot/Logs/" + logName + ".txt", "w")

		for line in lines:

			if line != lineToDelete:

				log.write(line)

		log.close()

	def removeCharFromString(self, string, char):

		self.string = string
		self.char = char
		strRetorno = ""

		for i in range(len(string)):

			if string[i] != char:

				strRetorno += string[i]

		return strRetorno


	def fixNumsWithKM(self, string):

		self.string = string
		hasK = False
		hasM = False
		strRetorno = ""

		for i in range(len(self.string)):
			
			if self.string[i] != "k" and self.string[i] != "m":
				
				strRetorno += string[i]

			elif self.string[i] == "k":

				hasK = True

			else:

				hasM = True

		if hasK:
			
			return (float(strRetorno) * 1000)

		elif hasM:

			return (float(strRetorno) * 1000000)

		else:

			return string

	